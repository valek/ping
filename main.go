package main

import (
	"fmt"
	"net"
	"os"
	"os/exec"
	"strconv"
	"time"

	"github.com/tatsushid/go-fastping"
	"github.com/vaughan0/go-ini"
)

func main() {

	// Загружаем конфиг файл
	file, err := ini.LoadFile("config.ini")
	if err != nil {
		fmt.Println("ini file not found")
		os.Exit(0)
	}

	// Получаем значение host из секции server
	host, ok := file.Get("server", "host")
	if !ok {
		panic("'host' variable missing from 'server' section")
	}

	// Получаем значение time из секции server
	time, ok := file.Get("server", "time")
	if !ok {
		panic("'time' variable missing from 'server' section")
	}

	// Конвертируем time.string в time.int64
	timeInt, err := strconv.ParseInt(time, 0, 64)
	if err != nil {
		fmt.Println(err)
		return
	}

	// Функция пинга (Через сколько пинговать, что пинговать)
	doEvery(timeInt, host)
}

func doEvery(d int64, host string) {
	for {
		time.Sleep(time.Duration(d) * time.Second)
		// f(time.Now())
		p := fastping.NewPinger()
		ra, err := net.ResolveIPAddr("ip4", host)
		if err != nil {
			fmt.Println(err)
			fmt.Println("Сервер перезагрузится через 5 секунд")
			runCommand("shutdown", "-r", "-t", "5")
			if err != nil {
				fmt.Println(err)
			}
			os.Exit(1)
		}
		p.AddIPAddr(ra)

		p.OnRecv = func(addr *net.IPAddr, rtt time.Duration) {
			fmt.Printf("IP Addr: %s receive, RTT: %v\n", addr.String(), rtt)
		}
		err = p.Run()
		if err != nil {
			fmt.Println(err)
		}
	}
}

//Функция запуска комманды в интерактивном режиме
func runCommand(cmdName string, arg ...string) {
	cmd := exec.Command(cmdName, arg...)
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	cmd.Stdin = os.Stdin
	err := cmd.Run()
	if err != nil {
		fmt.Printf("Failed to start. %s\n", err.Error())
		os.Exit(1)
	}
}
